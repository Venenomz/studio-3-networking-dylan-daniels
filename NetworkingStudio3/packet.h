#pragma once

#pragma pack(push,1)
class CursorInfo
{
public:
	CursorInfo() :m_posX(0), m_posY(0), m_data(0)
	{
	}
	unsigned short m_posX;
	unsigned short m_posY;
	unsigned char m_data;
};

class Packet
{
public:
	enum
	{
		e_pixel = 1,
		e_line,
		e_box,
		e_circle,
		e_clientAnnounce,
		e_clientCursor,
		e_serverInfo,
		e_serverCursors,
		e_serverAnnounce
	};
	Packet(int t = 0) : type(t) {}
	int type;
};

class PacketPixel : public Packet
{
public:
	PacketPixel() : Packet(e_pixel) {}
	int x;
	int y;
	float r;
	float g;
	float b;
};

class PacketBox : public Packet
{
public:
	PacketBox() : Packet(e_box) {}
	int x;
	int y;
	int w;
	int h;
	float r;
	float g;
	float b;
};

class PacketLine : public Packet
{
public:
	PacketLine() : Packet(e_line) {}
	int x1;
	int y1;
	int x2;
	int y2;
	float r;
	float g;
	float b;
};

class PacketCircle : public Packet
{
public:
	PacketCircle() : Packet(e_circle) {}
	int x;
	int y;
	int radius;
	float r;
	float g;
	float b;
};

class PacketClientAnnounce : public Packet
{
public:
	PacketClientAnnounce() : Packet(e_clientAnnounce) {}
	char name[50];
};

class PacketServerInfo : public Packet
{
public:
	PacketServerInfo() : Packet(e_serverInfo) {}
	unsigned short width;
	unsigned short height;
};

class PacketServerAnnounce : public Packet
{
public:
	PacketServerAnnounce() : Packet(e_serverAnnounce) {}
	unsigned short width;
	unsigned short height;
};

#pragma pack(pop)
