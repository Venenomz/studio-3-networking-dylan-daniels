////////////////////////////////////////////////////////////
// KGL - Kojack Graphics Library
// Copyright (C) 2016 Kojack (rajetic@gmail.com)
//
// KGL is released under the MIT License  
// https://opensource.org/licenses/MIT
////////////////////////////////////////////////////////////

#ifndef KGL_IMAGEIO_HEADER
#define KGL_IMAGEIO_HEADER

#include "kgl_image.h"
#include <filesystem>
#include "stb_image.h"
#include "stb_image_write.h"


namespace kgl
{
	bool loadHDR(kgl::Image& image, const std::filesystem::path &filename)
	{
		int width;
		int height;
		int channels;

		float *data = stbi_loadf(filename.generic_string().c_str(), &width, &height, &channels, 4);
		if (data)
		{
			image.create(width, height, 1);
			for (int y = 0; y < height; ++y)
			{
				for (int x = 0; x < width; ++x)
				{
					float* p = data+(x + y * width) * 4;
					image(x, y) = kf::ColourRGBA(p[0],p[1],p[2],p[3]);
				}
			}
			stbi_image_free(data);
			return true;
		}
		return false;
	}

	bool loadLDR(kgl::Image& image, const std::filesystem::path& filename)
	{
		int width;
		int height;
		int channels;

		stbi_uc *data = stbi_load(filename.generic_string().c_str(), &width, &height, &channels, 4);
		if (data)
		{
			image.create(width, height, 1);
			for (int y = 0; y < height; ++y)
			{
				for (int x = 0; x < width; ++x)
				{
					stbi_uc* p = data + (x + y * width) * 4;
					image(x, y) = kf::ColourRGBA(p[0] / 255.0f, p[1] / 255.0f, p[2] / 255.0f, p[3] / 255.0f);
				}
			}
			stbi_image_free(data);
			return true;
		}
		return false;
	}

	kf::u8* convertToInteger(kgl::Image& image, unsigned int z = 0)
	{
		kf::u8* data = new kf::u8[image.getWidth() * image.getHeight() * 4];
		for (int y = 0; y < image.getHeight(); ++y)
		{
			for (int x = 0; x < image.getWidth(); ++x)
			{
				kf::ColourRGBA c = image(x, y, z);
				data[(x + y * image.getWidth()) * 4 + 0] = kf::saturate(c.r) * 255.0f;
				data[(x + y * image.getWidth()) * 4 + 1] = kf::saturate(c.g) * 255.0f;
				data[(x + y * image.getWidth()) * 4 + 2] = kf::saturate(c.b) * 255.0f;
				data[(x + y * image.getWidth()) * 4 + 3] = kf::saturate(c.a) * 255.0f;

			}
		}
		return data;
	}

	bool save(kgl::Image& image, const std::filesystem::path& filename, unsigned int z=0, int quality = 100)
	{
		std::filesystem::path ext = filename.extension();
		if (ext == ".png")
		{
			kf::u8* data = convertToInteger(image, z);
			int result = stbi_write_png(filename.generic_string().c_str(), image.getWidth(), image.getHeight(), 4, data, 4 * image.getWidth());
			delete[] data;
			if (result)
				return true;
		}
		if (ext == ".bmp")
		{
			kf::u8* data = convertToInteger(image, z);
			int result = stbi_write_bmp(filename.generic_string().c_str(), image.getWidth(), image.getHeight(), 4, data);
			delete[] data;
			if (result)
				return true;
		}
		if (ext == ".tga")
		{
			kf::u8* data = convertToInteger(image, z);
			int result = stbi_write_tga(filename.generic_string().c_str(), image.getWidth(), image.getHeight(), 4, data);
			delete[] data;
			if (result)
				return true;
		}
		if (ext == ".jpg")
		{
			kf::u8* data = convertToInteger(image, z);
			int result = stbi_write_jpg(filename.generic_string().c_str(), image.getWidth(), image.getHeight(), 4, data, quality);
			delete[] data;
			if (result)
				return true;
		}
		if (ext == ".hdr")
		{
			//kf::u8* data = convertToInteger(image);
			int result = stbi_write_hdr(filename.generic_string().c_str(), image.getWidth(), image.getHeight(), 4, (float *)(image.m_buffer+z*image.getWidth()*image.getHeight()*4));
			//delete[] data;
			if (result)
				return true;
		}
		return false;
	}

}

#endif

