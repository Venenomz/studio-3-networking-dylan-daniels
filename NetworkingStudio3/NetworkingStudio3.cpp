// NetworkingStudio3.cpp : This file contains the 'main' function. Program execution begins and ends there.
#include <WinSock2.h>
#include <MSWSock.h>
#include <WS2tcpip.h>
#include <iostream>
#include <ostream>
#include "packet.h"
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "kgl/kgl_image.h"
#include "kgl/kgl_imageio.h"
#pragma comment(lib, "ws2_32.lib")


int main()
{
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		std::cout << "Failed" << std::endl;
		return 1;
	}

	unsigned short port = 1360;

	sockaddr_in server_address;

	// IPv4 address family
	server_address.sin_family = AF_INET;

	// Packets will be sent to 192.168.0.1.
	InetPton(AF_INET, "172.104.182.101",
		&server_address.sin_addr.s_addr);

	// The port number we are listening to.
	server_address.sin_port = htons(port);

	SOCKET mySocket = socket(AF_INET, SOCK_DGRAM, 0);
	if (mySocket == SOCKET_ERROR)
	{
		std::cout << "Error Opening socket" << std::endl;
		return 1;
	}

	DWORD dwBytesReturned = 0;
	bool bNewBehavior = false;
	WSAIoctl(mySocket,

		SIO_UDP_CONNRESET,
		&bNewBehavior,
		sizeof(bNewBehavior),
		NULL,
		0,
		&dwBytesReturned,
		NULL,
		NULL);


	PacketClientAnnounce pca;
	sendto(mySocket, (const char*)&pca, sizeof(PacketCircle), 0, (SOCKADDR*)&server_address, sizeof(server_address));

	char buffer[10000];

	Packet* p;

	sockaddr_in from;

	int fromlength = sizeof(from);

	int size = recvfrom(mySocket, buffer, sizeof(buffer), 0,
		(SOCKADDR*)&from, &fromlength);

	p = (Packet*)buffer;

	switch (p->type)
	{
	case Packet::e_serverInfo:
	{
		PacketServerInfo* psi = (PacketServerInfo*)p;

		//do something with sa

		int windowH = psi->height;
		int windowW = psi->width;

		PacketCircle pc;
		pc.x = windowW - 100;
		pc.y = windowH - 100;
		pc.radius = 99;
		pc.r = 1.0f;
		pc.g = 0;
		pc.b = 0;

		int size = sendto(mySocket, (const char*)&pc, sizeof(PacketCircle), 0, (SOCKADDR*)&server_address, sizeof(server_address));

		break;
	}
	}

	PacketPixel pp;

	kgl::Image image;

	kgl::loadLDR(image, "beaned.jpg");

	int height = image.getHeight();
	int width = image.getWidth();

	// create a box
	PacketBox pb;
	pb.x = 0;
	pb.y = 0;
	pb.w = width;
	pb.h = height;
	pb.r = 0;
	pb.g = 0;
	pb.b = 0;

	sendto(mySocket, (const char*)&pb, sizeof(PacketBox), 0, (SOCKADDR*)&server_address, sizeof(server_address));

	kf::ColourRGBA pixelColour;

	int result;

	// make pixels at the width and height
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			Sleep(2);

			pp.x = x;
			pp.y = y;
			pixelColour = image(x, y, 0);

			pp.r = pixelColour.r;
			pp.g = pixelColour.g;
			pp.b = pixelColour.b;

			result = sendto(mySocket, (const char*)&pp, sizeof(PacketPixel), 0, (SOCKADDR*)&server_address, sizeof(server_address));
		}
	}

	closesocket(mySocket);

	WSACleanup();

	return 0;
}

