////////////////////////////////////////////////////////////
// KF - Kojack Framework
// Copyright (C) 2019 Kojack (rajetic@gmail.com)
//
// KF is released under the MIT License  
// https://opensource.org/licenses/MIT
////////////////////////////////////////////////////////////

#ifndef KF_BOUNDS_HEADER
#define KF_BOUNDS_HEADER

#include "kf/kf_bounds1.h"
#include "kf/kf_bounds2.h"
#include "kf/kf_bounds3.h"


#endif

