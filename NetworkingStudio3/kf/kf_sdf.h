////////////////////////////////////////////////////////////
// KF - Kojack Graphics Library
// Copyright (C) 2019 Kojack (rajetic@gmail.com)
//
// KF is released under the MIT License  
// https://opensource.org/licenses/MIT
////////////////////////////////////////////////////////////
// SDF functions based on Inigo Quilez's articles at:
// https://www.iquilezles.org/www/articles/distfunctions2d/distfunctions2d.htm
// https://www.iquilezles.org/www/articles/distfunctions/distfunctions.htm
////////////////////////////////////////////////////////////

#ifndef KF_SDF_HEADER
#define KF_SDF_HEADER

#include "kf/kf_types.h"
#include "kf/kf_math.h"
#include "kf/kf_vector.h"
#include <vector>
#include "kf/kf_log.h"
#include "algorithm"

#pragma warning( disable : 4201)

namespace kf
{
	// 2D Functions

	namespace sd
	{
		float circle(float radius, kf::Vector2 p)
		{
			return p.length() - radius;
		}

		float line(kf::Vector2f a, kf::Vector2f b, kf::Vector2f p)
		{
			kf::Vector2f pa = p - a;
			kf::Vector2f ba = b - a;
			float h = kf::clamp(kf::dot(pa, ba) / kf::dot(ba, ba), 0.0f, 1.0f);
			return length(pa-ba*h);
		}

		float box(kf::Vector2f size, kf::Vector2f p)
		{
			kf::Vector2f d = p.abs() - size;
			return length(maximum(d, kf::Vector2::ZERO())) + std::min(std::max(d.x, d.y), 0.0f);
		}

		float rhombus(kf::Vector2f size, kf::Vector2f p)
		{
			// Curently wrong
			kf::Vector2f q = p.abs();
			float h = kf::clamp((-2.0 * ndot(q, size) + ndot(size, size)) / dot(size, size), -1.0, 1.0);
			float d = length(q - 0.5 * size * kf::Vector2(1.0 - h, 1.0 + h));
			return d * sign(q.x * size.y + q.y * size.x - size.x * size.y);
		}

		float equilateralTriangle(kf::Vector2f p)
		{
			const float k = sqrt(3.0);
			p.set(fabs(p.x) - 1.0f, p.y + 1.0f / k);
			if (p.x + k * p.y > 0.0) 
				p = kf::Vector2(p.x - k * p.y, -k * p.x - p.y) / 2.0f;
			p.x -= kf::clamp(p.x, -2.0f, 0.0f);
			return -length(p) * sign(p.y);
		}

		float unevenCapsule(float r1, float r2, float h, kf::Vector2f p)
		{
			p.x = fabs(p.x);
			float b = (r1 - r2) / h;
			float a = sqrt(1.0 - b * b);
			float k = kf::dot(p, kf::Vector2(-b, a));
			if (k < 0.0) return length(p) - r1;
			if (k > a * h) return length(p - kf::Vector2(0.0, h)) - r2;
			return dot(p, kf::Vector2(a, b)) - r1;
		}

		float add(float d1, float d2) 
		{ 
			return std::min(d1, d2); 
		}

		float subtract(float d1, float d2) 
		{ 
			return std::max(-d1, d2); 
		}

		float intersect(float d1, float d2) 
		{ 
			return std::max(d1, d2);
		}

		float smoothAdd(float d1, float d2, float k) {
			float h = kf::clamp(0.5f + 0.5f * (d2 - d1) / k, 0.0f, 1.0f);
			return kf::lerp(d2, d1, h) - k * h * (1.0f - h);
		}

		float smoothSubtract(float d1, float d2, float k) {
			float h = kf::clamp(0.5f - 0.5f * (d2 + d1) / k, 0.0f, 1.0f);
			return kf::lerp(d2, -d1, h) + k * h * (1.0f - h);
		}

		float smoothIntersect(float d1, float d2, float k) {
			float h = kf::clamp(0.5f - 0.5f * (d2 - d1) / k, 0.0f, 1.0f);
			return kf::lerp(d2, d1, h) + k * h * (1.0f - h);
		}

		kf::Vector2f move(kf::Vector2f offset, kf::Vector2f p)
		{
			return p - offset;
		}

	}

	// 3D Functions
	inline float sDistanceSphere(const kf::Vector3 &p, float radius)
	{
		return p.length() - radius;
	}

	inline float sDistanceBox(const kf::Vector3 &p, const kf::Vector3 &size)
	{
		kf::Vector3 d = kf::Vector3::abs(p) - size;
		return std::min(std::max(d.x, std::max(d.y, d.z)), 0.0f) + kf::Vector3::maximum(d, kf::Vector3::ZERO()).length();
	}

	inline float sDistanceTorus(const kf::Vector3 &p, float innerRadius, float outerRadius)
	{
		return kf::Vector2(p.xz().length() - innerRadius, p.y).length() - outerRadius;
	}

	inline float sDistanceCylinder(const kf::Vector3 &p, const kf::Vector3 &c)
	{
		return (p.xz() - c.xy()).length() - c.z;
	}

	inline float sDistanceCone(const kf::Vector3 &p, const kf::Vector2 &c)
	{
		return c.dot(kf::Vector2(p.xy().length(), p.z));
	}

	inline float sDistanceCapsule(const kf::Vector3 &p, const kf::Vector3 &a, const kf::Vector3 &b, float radius)
	{
		kf::Vector3 pa = p - a;
		kf::Vector3 ba = b - a;
		float h = kf::clamp(pa.dot(ba) / ba.dot(ba), 0.0f, 1.0f);
		return (pa - ba*h).length() - radius;
	}

	inline float sdfUnion(float d1, float d2)
	{
		return std::min(d1, d2);
	}

	inline float sdfSubtraction(float d1, float d2)
	{
		return std::max(-d1, d2);
	}

	inline float sdfIntersection(float d1, float d2)
	{
		return std::max(d1, d2);
	}
}

#endif

