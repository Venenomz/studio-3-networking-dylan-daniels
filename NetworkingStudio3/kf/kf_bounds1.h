////////////////////////////////////////////////////////////
// KF - Kojack Framework
// Copyright (C) 2019 Kojack (rajetic@gmail.com)
//
// KF is released under the MIT License  
// https://opensource.org/licenses/MIT
////////////////////////////////////////////////////////////

#ifndef KF_BOUNDS1_HEADER
#define KF_BOUNDS1_HEADER

#include "kf/kf_types.h"
#include "kf/kf_math.h"
#include <limits>

namespace kf
{
	template<typename TT>
	class AABB1T
	{
	public:
		using TYPE = typename TT;

		TT minCorner;
		TT maxCorner;
		bool infinite;

		inline AABB1T() :minCorner(1000000), maxCorner(-1000000), infinite(false)
		{
		}

		inline AABB1T(TT value) : minCorner(value), maxCorner(value), infinite(false)
		{
		}

		inline AABB1T(TT value1, TT value2) : infinite(false)
		{
			if (value1 <= value2)
			{
				minCorner = value1;
				maxCorner = value2;
			}
			else
			{
				minCorner = value2;
				maxCorner = value1;
			}
		}

		inline AABB1T &operator=(TT v)
		{
			minCorner = v;
			maxCorner = v;
			infinite = false;
			return *this;
		}

		inline AABB1T &set(TT v)
		{
			minCorner = v;
			maxCorner = v;
			infinite = false;
			return *this;
		}

		inline AABB1T &set(TT x1, TT x2)
		{
			if (x1 <= x2)
			{
				minCorner = x1;
				maxCorner = x2;
			}
			else
			{
				minCorner = x2;
				maxCorner = x1;
			}
			infinite = false;
			return *this;
		}

		inline bool operator==(const AABB1T &aabb)
		{
			return (infinite == aabb.infinite) && (minCorner == aabb.minCorner) && (maxCorner == aabb.maxCorner);
		}

		inline bool operator<(const AABB1T &aabb)
		{
			return minCorner < aabb.minCorner;
		}

		inline AABB1T &merge(TT v)
		{
			if (!infinite)
			{
				minCorner = kf::minimum(minCorner, v);
				maxCorner = kf::minimum(maxCorner, v);
			}
			return *this;
		}

		inline AABB1T &merge(const AABB1T &aabb)
		{
			if (!infinite)
			{
				if (!aabb.infinite)
				{
					minCorner =  kf::minimum(minCorner, aabb.minCorner);
					maxCorner =  kf::minimum(maxCorner, aabb.maxCorner);
				}
				else
				{
					infinite = true;
				}
			}
			return *this;
		}

		inline TT size() const
		{
			if (!infinite)
			{
				return (maxCorner - minCorner);
			}
			else
			{
				return TT(0);
			}
		}

		inline TT centre() const
		{
			if (!infinite)
			{
				return (maxCorner + minCorner)*0.5;
			}
			else
			{
				return TT(0);
			}
		}

		inline static AABB1T ZERO()
		{
			return AABB1T(0, 0);
		}

		inline static AABB1T Infinite()
		{
			AABB1T aabb;
			aabb.infinite = true;
			return aabb;
		}
	};


	typedef AABB1T<float>  AABB1f;
	typedef AABB1T<int>    AABB1i;
	typedef AABB1T<double> AABB1d;
	typedef AABB1T<float>  AABB1;
}

#endif

