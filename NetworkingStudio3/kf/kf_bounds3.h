////////////////////////////////////////////////////////////
// KF - Kojack Framework
// Copyright (C) 2019 Kojack (rajetic@gmail.com)
//
// KF is released under the MIT License  
// https://opensource.org/licenses/MIT
////////////////////////////////////////////////////////////

#ifndef KF_BOUNDS3_HEADER
#define KF_BOUNDS3_HEADER

#include "kf/kf_types.h"
#include "kf/kf_math.h"
#include "kf/kf_vector3.h"
#include <limits>

namespace kf
{
	template<typename TT>
	class AABB3T
	{
	public:
		using TYPE = typename TT;

		Vector3T<TT> minCorner;
		Vector3T<TT> maxCorner;
		bool infinite;

		inline AABB3T() :minCorner(1000, -100000, 1000000), maxCorner(1000, 100000, -100000), infinite(false)
		{
		}

		explicit inline AABB3T(const Vector3T<TT> &v) : minCorner(v), maxCorner(v), infinite(false)
		{
		}

		explicit inline AABB3T(const Vector3T<TT> &v1, const Vector3T<TT> &v2) : infinite(false)
		{
			if (v1.x <= v2.x)
			{
				minCorner.x = v1.x;
				maxCorner.x = v2.x;
			}
			else
			{
				minCorner.x = v2.x;
				maxCorner.x = v1.x;
			}
			if (v1.y <= v2.y)
			{
				minCorner.y = v1.y;
				maxCorner.y = v2.y;
			}
			else
			{
				minCorner.y = v2.y;
				maxCorner.y = v1.y;
			}
			if (v1.z <= v2.z)
			{
				minCorner.z = v1.z;
				maxCorner.z = v2.z;
			}
			else
			{
				minCorner.z = v2.z;
				maxCorner.z = v1.z;
			}
		}

		inline AABB3T(TT x1, TT y1, TT z1, TT x2, TT y2, TT z2) : infinite(false)
		{
			if (x1 <= x2)
			{
				minCorner.x = x1;
				maxCorner.x = x2;
			}
			else
			{
				minCorner.x = x2;
				maxCorner.x = x1;
			}
			if (y1 <= y2)
			{
				minCorner.y = y1;
				maxCorner.y = y2;
			}
			else
			{
				minCorner.y = y2;
				maxCorner.y = y1;
			}
			if (z1 <= z2)
			{
				minCorner.z = z1;
				maxCorner.z = z2;
			}
			else
			{
				minCorner.z = z2;
				maxCorner.z = z1;
			}
		}

		inline AABB3T &operator=(const Vector3T<TT> &v)
		{
			minCorner = v;
			maxCorner = v;
			infinite = false;
			return *this;
		}

		inline AABB3T &set(const Vector3T<TT> &v)
		{
			minCorner = v;
			maxCorner = v;
			infinite = false;
			return *this;
		}

		inline AABB3T &set(const Vector3T<TT> &v1, const Vector3T<TT> &v2)
		{
			if (v1.x <= v2.x)
			{
				minCorner.x = v1.x;
				maxCorner.x = v2.x;
			}
			else
			{
				minCorner.x = v2.x;
				maxCorner.x = v1.x;
			}
			if (v1.y <= v2.y)
			{
				minCorner.y = v1.y;
				maxCorner.y = v2.y;
			}
			else
			{
				minCorner.y = v2.y;
				maxCorner.y = v1.y;
			}
			if (v1.z <= v2.z)
			{
				minCorner.z = v1.z;
				maxCorner.z = v2.z;
			}
			else
			{
				minCorner.z = v2.z;
				maxCorner.z = v1.z;
			}
			infinite = false;
			return *this;
		}

		inline AABB3T &set(TT x1, TT y1, TT z1, TT x2, TT y2, TT z2)
		{
			if (x1 <= x2)
			{
				minCorner.x = x1;
				maxCorner.x = x2;
			}
			else
			{
				minCorner.x = x2;
				maxCorner.x = x1;
			}
			if (y1 <= y2)
			{
				minCorner.y = y1;
				maxCorner.y = y2;
			}
			else
			{
				minCorner.y = y2;
				maxCorner.y = y1;
			}
			if (z1 <= z2)
			{
				minCorner.z = z1;
				maxCorner.z = z2;
			}
			else
			{
				minCorner.z = z2;
				maxCorner.z = z1;
			}
			infinite = false;
			return *this;
		}

		inline bool operator==(const AABB3T &aabb)
		{
			return (infinite == aabb.infinite) && (minCorner == aabb.minCorner) && (maxCorner == aabb.maxCorner);
		}

		inline bool operator<(const AABB3T &aabb)
		{
			return minCorner < aabb.minCorner;
		}

		inline AABB3T &merge(const Vector3T<TT> &v)
		{
			if (!infinite)
			{
				minCorner = minCorner.minimum(v);
				maxCorner = maxCorner.maximum(v);
			}
			return *this;
		}

		inline AABB3T &merge(const AABB3T &aabb)
		{
			if (!infinite)
			{
				if (!aabb.infinite)
				{
					minCorner = minCorner.minimum(aabb.minCorner);
					maxCorner = maxCorner.maximum(aabb.maxCorner);
				}
				else
				{
					infinite = true;
				}
			}
			return *this;
		}

		inline TT width() const
		{
			if (!infinite)
			{
				return (maxCorner.x - minCorner.x);
			}
			else
			{
				return 0;
			}
		}

		inline TT height() const
		{
			if (!infinite)
			{
				return (maxCorner.y - minCorner.y);
			}
			else
			{
				return 0;
			}
		}

		inline TT depth() const
		{
			if (!infinite)
			{
				return (maxCorner.z - minCorner.z);
			}
			else
			{
				return 0;
			}
		}

		inline Vector3T<TT> size() const
		{
			if (!infinite)
			{
				return (maxCorner - minCorner);
			}
			else
			{
				return kf::Vector3T<TT>::ZERO();
			}
		}

		inline Vector3T<TT> centre() const
		{
			if (!infinite)
			{
				return (maxCorner + minCorner)*0.5;
			}
			else
			{
				return kf::Vector3T<TT>::ZERO();
			}
		}

		inline static AABB3T ZERO()
		{
			return AABB3T(0, 0, 0, 0, 0, 0);
		}

		inline static AABB3T Infinite()
		{
			AABB3T aabb;
			aabb.infinite = true;
			return aabb;
		}
	};


	typedef AABB3T<float>  AABB3f;
	typedef AABB3T<int>    AABB3i;
	typedef AABB3T<double> AABB3d;
	typedef AABB3T<float>  AABB3;
}

#endif

